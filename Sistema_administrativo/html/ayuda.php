<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>ayuda</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>
  <?php
  session_start();
  include('cabecera.php');
  ?>     <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li title="Iras a la pantalla principal del sistema"><a href="../html/menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
                        
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li><a href="../php/respaldo.php">Respaldar la Base de Datos </a></li>
                                <li><a href="../php/respaldo.php">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.html"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
				
				<div class="span9">
					<div class="hero-unit">
						<h2 class="text-center">Ayuda</h2>
                                                <a id="top"></a>
						<p>Aqui tienes la principales Preguntas y respuestas de como usar el Sistema de Administrativo de Punto Libre.
						</p>
				
                                        <video width="auto" height="auto" controls>

  <source src="melda.mp4" type="video/mp4">

  Your browser does not support the video tag.
</video>

					
		</div>

        <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        
       
      

<script type="text/javascript">
${"dropdown-toggle"}.dropdown{}
       
  </script>

  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
<script type="text/javascript">
    $('#myModal').on('hidden', function () {
    // do something…
    })</script>


</body>
</html>