<?php
session_start();
include("../conf.php");
include("../php/lib/conexion.php");
$con=conexion();
include("../php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Insertar_Informacion']==1)

{
?>
  <!DOCTYPE html>
<html lang="es">
 <head>
    <meta charset="utf-8" />
    <title>Sistema Administrativo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.min.css" media="screen"/>  
<link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css" />
  </head>
 <body>
<?php
include("../html/cabecera.php");
?>

             <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav  affix"">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                             <li class="active" title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="../html/menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="../html/ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="../html/reporte.php"><i class="icon-file"></i> Reportes</a></li>
                             <li title="Modificacion y deshabilitacion de los Puntos"><a href="../html/modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y Status P.L</a></li>
              
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li><a href="../php/respaldo.php">Respaldar la Base de Datos </a></li>
                                <li><a href="../php/respaldo.php">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.php"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>
<div class='span9'>
	<div class='hero-unit'>
		<div class='row-fluid'>
                    <div class='span12 text-center btn-primary'>
			<span>Respaldo y Restauracion de Base de Datos del Sistema</span>
                        </div>
			
			<div class='row-fluid'>
				<div class="span12 text-center">
				    <span>Respaldar la información registrada en el Sistema</span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 text-center">
<input type=button value='Respaldar' class="btn btn-primary" TITLE='respalda base de datos del sistema' onClick=location.href='../respaldos/respaldar.php?act=respaldar'>
				</div>
			</div>
                        <br>
			<div class='row-fluid'>
				<div class='span12 text-center btn-primary'>
				    <span>Restaurar Información</span>
				</div>
			</div>
			<div class='row-fluid'>
				<div class="span12 text-center">
				    <span>Restaurar la información con un archivo de respaldo guardado</span>
				</div>
			</div>
			<form class="text-center" action='../respaldos/respaldar.php?act=aplicarresp' method='post' enctype='multipart/form-data'>
<input name='archivo' type='file' size='20'/>
<input name='enviar' type='submit' class="btn btn-primary" TITLE='restaura base de datos del sistema' value='Restaurar' />
<input name='action' type='hidden' value='upload' />     
			</form>
		</div>
	</div>
</div>
</div>
</div>
              <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
  <script type="text/javascript">
  ${"dropdown-toggle"}.dropdown{}
         
    </script>
  

  
    <script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.10.3.custom.js"></script>
<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para ingresar al respaldo de la base de datos.');
                      document.location=('../html/paginaprincipal.php');
                  </script>";
		  
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
?>






