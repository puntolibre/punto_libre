<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
?>
	<div class="span9">
	  <div class="hero-unit">
	    <h3 class="text-center">Mobiliario</h3>
	    <form method="post" action="insertar_mobiliario.php">
           <div class="row-fluid">
	    <div class="span12 text-center btn-primary">
	      <span>Datos del Bien</span>
	    </div>
	  </div><br />
	  <div class="row-fluid">
	    <div class="span3">
		Código del Bien:
	    </div>
	    <div class="span3">
	      <input type="text" class="text-center" name="cod_bien" placeholder="MB000" title="Introducir Código del Bien" pattern="[MB]{2}[0-9]{3}" size="11" required/>
	    </div>
	    <div class="span2">
	     Fecha:
	    </div>
	    <div class="span3 control-group">
	      <div class="controls input-prepend date form_date" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd"
		   data-link-field="fecha_adq" data-link-format="yyyy-mm-dd">
		  <span class="add-on"><i class="icon icon-calendar"></i></span>
		  <input size="10" type="text" value="" required>
	      </div>
	      <input type="hidden" id="fecha_adq" name="fecha_adq" value="" /><br/>
	    </div>
	  </div>
	  <div class="row-fluid">
	     <div class="span3">
		Descripción del Bien:
	     </div>
	     <div class="span8">
		<textarea name="descripcion" class="input-block-level"  placeholder="Indique el nombre característico de bien" rows="5" cols="60" required></textarea>
	     </div>
	  </div>
	  <div class="row-fluid">
	    <div class="span3">
	       Adquisición:
	    </div>
	    <div class="span4">
	      Compra <input type="radio" name="forma_adq" value="Compra" onclick=" precio.disabled=false" checked/>
	      Donación <input type="radio" name="forma_adq" value="Donacion" onclick="precio.disabled=true"/>
	     </div>
	  </div>
	  <div class="row-fluid">
	      <div class="span3">
		Proveedor:
	      </div>
	      <div class="span3">
		 <input type="text" name="proveedor" size="20" class="input-block-level" required title="Introducir el Proveedor">
	      </div>
	  </div>
	  <div class="row-fluid">
	      <div class="span3">
		  Precio (IVA Incluido):
	      </div>
	      <div class="span3 input-prepend input-append">
	        <span class="add-on">Bs F.</span>
		<input type="text" class="text-center" name="precio" size="8" maxlength="5" pattern="[0-9]{0,}" placeholder="000" required/>
		<span class="add-on">.00</span>
	      </div>     
	  </div><br>
	      <div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span>Especificaciones</span>
		</div>
	      </div><br>
	      <div class="row-fluid text-center">
		<div class="span3"></div>
		<div class="span2">
		      Marca:
		      <input type="text" name="marca_mobiliario" title="Introducir la Marca" class="input-block-level"/>
		</div>
		<div class="span2">
		      Modelo: <input type="text" name="modelo_mobiliario" title="Introducir el Modelo" class="input-block-level"/>
		</div>
		<div class="span2">
		      Serial: <input type="text" name="serial_mobiliario" required class="input-block-level" pattern="[A-Z]{1}[-][0-9]{3}[#]" title="Indicar la Incial del bien específico seguido del número de fabrica" placeholder=" M-123#"/>
		</div>
	      </div><br>
	      <div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span>Características</span>
		</div>
	      </div><br />
	      <div class="row-fluid text-center">
		<div class="span3"></div>
		<div class="span2">
			Material:
		      <select name="material_mobiliario" required title="Seleccionar tipo de Material">
			<option></option>
			<option value="MADERA">MADERA</option>
			<option value="HIERRO">HIERRO</option>
			<option value="PLASTICO">PLASTICO</option>
		    </select>
		</div>
		<div class="span2">
		      Color:
		         <select name="color_mobiliario" required title="Seleccionar Color">
			<option></option>
			<option value="MARRON">MARRON</option>
			<option value="PLOMO">PLOMO</option>
			<option value="PLATEADO">PLATEADO</option>
			<option value="NEGRO">NEGRO</option>
			 </select>
		</div>
		<div class="span2">
		      Dimensión: <input type="text" tittle="Especificar la dimensión del Mobiliario" name="dimension_mobiliario" placeholder="1m x 0.8m" class="input-block-level"/>
		</div>
	      </div><hr>
	      <div class="row-fluid text-center">
		<div class="span12">
		    Observación:
		    <textarea name="observacion" class="input-block-level" placeholder="Indique el tipo de obsevacion que desea registrar del bien a incorporar" cols="70" rows="5"></textarea>
		</div>
	      </div>
	      <hr><br />
	      <div class="row-fluid">
		<div class="span12 text-center">
		<button type="submit" class="btn btn-primary"><i class="icon-hdd icon-white"></i> Registrar</button>
		<button type="reset" class="btn btn-warning"><i class="icon-trash icon-white"></i> Limpiar</button>
		<a class="btn btn-danger" href="#myModal" data-toggle="modal" role="button"><i class="icon-remove icon-white"></i> Cancelar</a>
		</div>
	      </div>
	    </form>
	  </div>
	</div>
      </div>
    </div>
     <!-- ----------------------------------------------------------------------------------------------------- -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header ">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="myModalLabel">Advertencia de Seguridad</h3>
</div>
<div class="modal-body alert alert-block">
<strong><p>¿Esta seguro que desea cancelar el registro?</p></strong>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true"><i class="icon-share-alt"></i> Volver</button>
<button type="submit" class="btn btn-danger" onclick="location.href='planilla_recepcion.php'"><i class="icon-remove icon-white"></i> Cancelar</button>
</div>
</div>
<!-- ----------------------------->
    <?php
require('piepagina.php');
?>

