<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Sistema de Control de Inventario de Bienes Muebles</title>
    <meta name="autor" content="Maria Teran, Iris Diaz, Holgui Vega" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-responsive.css" media="screen">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-datetimepicker.min.css" media="screen"/>
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/validacion.js"></script>
  </head>
  <body>
    <div class="wrapper">
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
	<div class="container-fluid">
	  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	  </button>
	  <a class="brand" href="#"><img src="../img/PuntoLibre.svg" width='20' height='20'/>Punto Libre</a>
	  <div class="nav-collapse collapse">
	    <ul class="nav">
	      <li><a href="../../Sistema_administrativo/html/menu_definitivo.php"><i class="icon-home"></i> Administración de P.L.</a></li>
	      <li><a href="../../sme/html/index.php"><i class="icon-book"></i> Gestion de Cursos</a></li>
	      <li class="active dropdown">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-hdd"></i> Control de Inventario<b class="caret"></b></a>
		  <ul class="dropdown-menu">
		      <li><a href="planilla_recepcion.php" ><i class="icon-pencil"></i> Registro de Bienes </a></li>
		      <li><a href="planilla7_porTipo.php"><i class="icon-search "></i> Consulta por Tipo</a></li>
		      <li><a href="planilla8_estatus.php"><i class="icon-search "></i> Consulta por Estatus</a></li>
		  </ul>
	      </li>
	      <li><a href="../../gs/html/index.php"><i class="icon-tasks"></i> Soporte a Equipos</a></li>
	    </ul>
	    <div class="pull-right">
	      <ul class="nav pull-right">
		<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i><?php echo  $_SESSION['usuario']; ?><b class="caret"></b></a>
		  <ul class="dropdown-menu">
		      <li><a href="#"><i class="icon-user"></i>Perfil</a></li>
		      <li><a href="#"><i class="icon-headphones"></i>Ayuda General</a></li>
		      <li class="divider"></li>
		      <li><a href="#"><i class="icon-off"></i>Cerrar sesión</a></li>
		  </ul>
		</li>
	      </ul>
	    </div>
	  </div>
	</div>
      </div>
    </div>
   <br>
