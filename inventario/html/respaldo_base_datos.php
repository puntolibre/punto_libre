<?php
require("cabecera.php");
require("menu.php");
require('conexion.php');
?>
<div class="span9"><!--Contenido-->
	<div class="hero-unit"><!--Bloque de Contenido Gris-->
		<div class='row-fluid'>
			<div class='span12 text-center btn-primary'>
			    <span>Respaldar Información</span>
			</div>
		</div><br>
		<div class='row-fluid'>
			<div class="span12 text-center">
			    <span>Respaldar la base de datos del Sistema</span>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12 text-center">
			<a class="btn" href="#myModal" title='Respaldar base de datos del sistema' data-toggle="modal"><i class="icon-download-alt"></i> Respaldar</a>
			</div>
		</div><hr>
	</div>
</div>
</div>
</div>
<!-- ----------------------------------------------------------------------------------------------------- -->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header ">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="myModalLabel">Advertencia de Seguridad</h3>
</div>
<div class="modal-body alert alert-block">
<strong><p>¿Desea hacer un Respaldo de la Base de Datos.?</p></strong>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
<button type="submit" class="btn btn-primary" onclick="location.href='respaldar.php?act=respaldar'"><i class="icon-download-alt icon-white"></i>Respaldar</button>
</div>
</div>
<!-- ----------------------------------------------------------------------------------------------------- -->
<?php
require("piepagina.php");
?>