-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-06-2014 a las 16:15:24
-- Versión del servidor: 5.5.37
-- Versión de PHP: 5.4.4-14+deb7u9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `inventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bien`
--

CREATE TABLE IF NOT EXISTS `bien` (
  `cod_bien` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_adq` date NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `forma_adq` enum('Donacion','Compra') COLLATE utf8_spanish_ci NOT NULL,
  `proveedor` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `precio` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `status` enum('Incorporado','Averiado','Desincorporado') COLLATE utf8_spanish_ci NOT NULL,
  `cod_registro` int(4) NOT NULL,
  PRIMARY KEY (`cod_bien`),
  KEY `cod_registro` (`cod_registro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `bien`
--

INSERT INTO `bien` (`cod_bien`, `fecha_adq`, `descripcion`, `forma_adq`, `proveedor`, `precio`, `status`, `cod_registro`) VALUES
('PR001', '2014-06-15', 'Disco Duro', 'Donacion', 'CNTI', '', 'Incorporado', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `componente`
--

CREATE TABLE IF NOT EXISTS `componente` (
  `marca_componente` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `modelo_componente` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `serial_componente` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `observacion_componente` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_bien` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  KEY `cod_bien` (`cod_bien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumo_limpieza`
--

CREATE TABLE IF NOT EXISTS `insumo_limpieza` (
  `marca_limpieza` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `serial_limpieza` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `observacion_limpieza` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_bien` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  KEY `cod_bien` (`cod_bien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumo_oficina`
--

CREATE TABLE IF NOT EXISTS `insumo_oficina` (
  `marca_oficina` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `serial_oficina` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `observacion_oficina` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_bien` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  KEY `cod_bien` (`cod_bien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mobiliario`
--

CREATE TABLE IF NOT EXISTS `mobiliario` (
  `marca_mobiliario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `modelo_mobiliario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `serial_mobiliario` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `material_mobiliario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `color_mobiliario` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `dimension_mobiliario` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `observacion_mobiliario` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_bien` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  KEY `cod_bien` (`cod_bien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pc`
--

CREATE TABLE IF NOT EXISTS `pc` (
  `marca` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `dd` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `serial` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `ram` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `placa` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `unidad` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `marca_monitor` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `color_monitor` varchar(7) COLLATE utf8_spanish_ci NOT NULL,
  `pulgada` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `marca_procesador` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `ghz` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `color_mouse` varchar(7) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_mouse` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `color_teclado` varchar(7) COLLATE utf8_spanish_ci NOT NULL,
  `marca_teclado` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `estado_bien` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `observacion` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_bien` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  UNIQUE KEY `cod_bien` (`cod_bien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periferico`
--

CREATE TABLE IF NOT EXISTS `periferico` (
  `marca_periferico` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `modelo_periferico` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `serial_periferico` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `observacion_periferico` text COLLATE utf8_spanish_ci NOT NULL,
  `cod_bien` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  KEY `cod_bien` (`cod_bien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `periferico`
--

INSERT INTO `periferico` (`marca_periferico`, `modelo_periferico`, `serial_periferico`, `observacion_periferico`, `cod_bien`) VALUES
('Samsung', 'Portatil', 'D-001#', 'Disco Duro de 1TB', 'PR001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recepcion`
--

CREATE TABLE IF NOT EXISTS `recepcion` (
  `cod_registro` int(4) NOT NULL AUTO_INCREMENT,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `estado` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `municipio` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `sede` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`cod_registro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `recepcion`
--

INSERT INTO `recepcion` (`cod_registro`, `fecha_registro`, `estado`, `municipio`, `sede`) VALUES
(18, '2014-06-15 08:43:28', 'MÃ©rida', 'Libertador', 'Laboratorio Ipostel Centro');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bien`
--
ALTER TABLE `bien`
  ADD CONSTRAINT `fk_bien_recepcion` FOREIGN KEY (`cod_registro`) REFERENCES `recepcion` (`cod_registro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `componente`
--
ALTER TABLE `componente`
  ADD CONSTRAINT `fk_componente_bien` FOREIGN KEY (`cod_bien`) REFERENCES `bien` (`cod_bien`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `insumo_limpieza`
--
ALTER TABLE `insumo_limpieza`
  ADD CONSTRAINT `fk_insumo_limpieza_bien` FOREIGN KEY (`cod_bien`) REFERENCES `bien` (`cod_bien`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `insumo_oficina`
--
ALTER TABLE `insumo_oficina`
  ADD CONSTRAINT `fk_insumo_oficina_bien` FOREIGN KEY (`cod_bien`) REFERENCES `bien` (`cod_bien`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mobiliario`
--
ALTER TABLE `mobiliario`
  ADD CONSTRAINT `fk_mobiliario_bien` FOREIGN KEY (`cod_bien`) REFERENCES `bien` (`cod_bien`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pc`
--
ALTER TABLE `pc`
  ADD CONSTRAINT `fk_pc_bien` FOREIGN KEY (`cod_bien`) REFERENCES `bien` (`cod_bien`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `periferico`
--
ALTER TABLE `periferico`
  ADD CONSTRAINT `fk_periferico_bien` FOREIGN KEY (`cod_bien`) REFERENCES `bien` (`cod_bien`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
