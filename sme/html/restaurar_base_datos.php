<?php
require("cabecera.php");
require("menu.php");
require('conexion.php');

?>
<div class='span9'>
	<div class='hero-unit'>
		<div class='row-fluid'>
			<div class='span12 text-center btn-primary'>
			    <span>Restaurar Base de Datos</span>
			</div>
		</div><br>
		<div class='row-fluid'>
			<div class="span12 text-center">
			    <span>Restaurar base de datos de un archivo de respaldo guardado</span>
			</div>
		</div><br>
		<div class="row-fluid">
		 <div class="span12 text-center">
		 	<form action='respaldar.php?act=aplicarresp' method='post' enctype='multipart/form-data'>
				<input name='archivo' type='file' size='20'/>
				<input name='enviar' type='submit' title='Restaurar la base de datos seleccionada' id='boton' value='Restaurar' />
				<input name='action' type='hidden' value='upload' />     
			</form>
		 </div>
		</div><hr>
	</div><!--cierre del Hero-Unit-->
</div><!--cierre del contenido-->
</div><!--cierre del row-fluid de contenido-->
</div><!--cierre del container-->
<?php
require("piepagina.php");
?>