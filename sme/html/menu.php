	<div class="container-fluid"><!--Container de todo el cuerpo-->
	    <div class="row-fluid"><!--row-fluid del contenido-->
		<div class="span3 ">
		    <div class="well affix">
			<ul class="nav nav-pills nav-stacked">
			    <li class="nav-hearder text-center"><h4>Gestion de Cursos</h4></li>
			    <li class="dropdown ">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-hdd"></i> Curso<b class="caret"></b></a> 
				<ul class="dropdown-menu">
				    <li><a href="curso_registro.php"><i class="icon-file"></i> Registrar</a></li>
				    <li><a href="curso_consultar.php"><i class="icon-search"></i> Consultar</a></li>
				</ul>	
			    </li>
			    <li class="dropdown ">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-user"></i> Participante<b class="caret"></b></a> 
				<ul class="dropdown-menu ">
				    <li><a href="participante_registro.php"><i class="icon-file"></i> Registrar</a></li>
				    <li><a href="participante_consultar.php"><i class="icon-search"></i> Consultar</a></li>
				</ul>	
			    </li>
			    <li class="dropdown ">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-user"></i> Instructor<b class="caret"></b></a> 
				<ul class="dropdown-menu ">
				    <li><a href="instructor_registrar.php"><i class="icon-file"></i> Registrar</a></li>
				    <li><a href="instructor_consultar.php"><i class="icon-search"></i> Consultar</a></li>
				</ul>	
			    </li>
			    <li><a href="inscripcion.php"><i class="icon-pencil"></i> Inscripción</a></li>
			    <li class="dropdown ">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
				<ul class="dropdown-menu ">
				    <li class="disabled"><a href="#"><i class="icon-hdd"></i> Reporte</a></li>
				    <li><a href="respaldo_base_datos.php"><i class="icon-download-alt"></i> Respaldar</a></li>
				    <li><a href="restaurar_base_datos.php"><i class="icon-upload"></i> Restaurar</a></li>
				    <li class="divider"></li>
				    <li><a href="#"><i class="icon-headphones"></i> Ayuda</a></li>
				</ul>	
			    </li>
			</ul>
		    </div>
		</div>