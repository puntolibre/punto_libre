<div class="push"></div>
</div><!--Div del Wrapper-->    
    <div class="footer-azul"><!--Pie de pagina-->
	<div class="container">
	    <div class="row-fluid">
		<div class="span12">
		    <div class="span4">
			<p class="muted"><a href="http://puntolibre.gnu.org.ve/" target="_blank"><img src="../img/punto-small.png"/> Punto Libre 2013 <i class="icon-globe"></i></a></p>
		    </div>
		<!--<div class="span6">
			Algunos derechos reservados <img alt="" src="../img/Copyleft.png" width='15' height='15'/></a>
			<br /> Esta constituido bajo una licencia de <a rel="license" href="http://creativecommons.org/licenses/by/3.0/deed.es" title="Creative Commons Attribution 3.0 Unported">Creative Commons Attribution 3.0 Unported</a>.<br />
		    </div>-->
		    <div class="span4">
			<p class="muted"><a href="http://gnu.org.ve/" target="_blank"><img src="../img/gnu.png" width='25' height='25'/> Proyecto GNU Venezuela 2013 <i class="icon-globe"></i></a></p>
		    </div>
		    <div class="span4">
		    <p class="muted"><a href="http://www.uptm.edu.ve" target="_blank">Universidad Politécnica Territorial del Estado Mérida <br/>"Kléber Ramírez"<i class="icon-globe"></i> </a></p>
		    </div>

		</div>
	    </div>
	</div>
    </div>
<!--script del calendario y la hora-->
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>
    <script type="text/javascript">
	$('.form_date').datetimepicker
	({
	   language:  'es',
	   weekStart: 1,
	   todayBtn:  1,
		   autoclose: 1,
		   todayHighlight: 1,
		   startView: 2,
		   minView: 2,
		   forceParse: 0,
		   daysOfWeekDisabled: [0],
		   startDate: "<?php echo (date('Y-m-d'))?>"
		   
       });
	   $('.form_time').datetimepicker
	   ({
	   language:  'es',
	   weekStart: 1,
	   todayBtn:  1,
		   autoclose: 1,
		   todayHighlight: 1,
		   startView: 1,
		   minView: 0,
		   maxView: 1,
		   forceParse: 0
       });
    </script>
</body>
</html>
