<?php
     require('cabecera.php');
     require('menu.php');
     
$con= @mysql_connect("localhost","root","21183652");
     
     if(!mysql_select_db("administracion", $con)){
         echo "error al conectarse";}
 include("../../Sistema_administrativo/php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Acceso_Cursos']==1 and $_SESSION['Insertar_Informacion']==1){    
?>
<div class="span9"><!--Contenido-->
     <div class="hero-unit"><!--Bloque de Contenido Gris-->
	  <h3 class="text-center">Registro de Nuevo Curso</h3>
	       <form method="POST" action="curso_insertar.php" enctype='multipart/form-data'><!--incio de formulario-->
		    <div class="row-fluid">
			<div class="span12 text-center btn-primary">
			    <span>Datos del Curso</span>
			</div>
		    </div>
		    <div class="row-fluid">
			 <div class="span12"><br />
			      <div class="row-fluid">
				  <div class="span2"><span>Curso:</span></div>
				  <div class="span7">
				      <input type="text" name="nombre_curso" class="input-block-level" maxlength="100" placeholder="Curso del Punto Libre" title="Solo debe introducir letras" pattern="[A-Z\a-z\á,é,í,ó,ú ]{0,}[5]{0,1}" required>
				  </div>
			      </div>
			      <div class="row-fluid">
				   <div class="span2"><span>Imagen:</span></div>
				   <div class="span7">
				      <input type="file" class="input-block-level" name="archivo" required/>
				   </div>
			      </div><br>
			      <div class="row-fluid">
				  <div class="span2"><span>Descripción:</span></div>
				  <div class="span7">
					<textarea name="descripcion_curso" class="input-block-level"  rows="5" placeholder="Descripción del Curso" title="Ingrese la descripcion del curso" required></textarea>
				  </div>
			      </div>
			      <div class="row-fluid">
				  <div class="span2"><span>Temas:</span></div>
				  <div class="span7">					    
				      <textarea name="tema_curso" class="input-block-level" rows="5" placeholder="Temas del Curso" title="Ingrese los temas del curso" required></textarea>
				  </div>
			      </div>
			 </div>
		    </div>
		    <div class="row-fluid">
			 <div class="span12 text-center btn-primary">
			     <span>Horario del Curso</span>
			 </div>
		    </div><br />
		    <div class="row-fluid">
			 <div class="span2">
			      <span>Fecha inicio:</span>
			 </div>
			 <div class="span3">
			      <div class="control-group">
				   <div class="controls input-append date form_date" data-date="<?php echo date('Y-m-d');?>" data-date-format="dd MM yyyy" data-link-field="fechaini_curso" data-link-format="yyyy-mm-dd">
					<input type="text" size="10" value="" class="input-block-level" readonly>
					<span class="add-on"><i class="icon icon-calendar"></i></span>
				   </div>
				   <input type="hidden" id="fechaini_curso" name="fechaini_curso"/><br/>
			      </div>
			 </div>
			 <div class="span2">
			      <span>Fecha Fin:</span>
			 </div>	 
			 <div class="span3">
			      <div class="control-group">
				   <div class="controls input-prepend date form_date" data-date="<?php echo date('Y-m-d');?>" data-date-format="dd MM yyyy" data-link-field="fechafin_curso" data-link-format="yyyy-mm-dd">
					<span class="add-on"><i class="icon icon-calendar"></i></span>
					<input type="text" size="10" value="" class="input-block-level" readonly>
				   </div>
				   <input type="hidden" id="fechafin_curso" name="fechafin_curso"/><br/>
			      </div>
			 
			 </div>
		    </div>
		    <div class="row-fluid">
			 <div class="span2">
				  <span>Hora Inicio:</span>
			 </div>
			 <div class="span3">
			      <div class="control-group">
				   <div class="controls input-append date form_time" data-date="" data-date-format="hh:ii:ss"  data-link-field="horaini_curso" data-link-format="hh:ii:ss">
					<input type="text" value="" size="13" readonly>
					<span class="add-on"><i class="icon-time"></i></span>
				   </div>
				   <input type="hidden" id="horaini_curso" name="horaini_curso" class="input-block-level"/><br/>
			      </div>
			 </div>
			 <div class="span2">
			      <span>Hora Fin:</span>
			 </div>
			 <div class="span4">
			      <div class="control-group">
				   <div class="controls input-prepend date form_time" data-date="" data-date-format="hh:ii:ss"  data-link-field="horafin_curso" data-link-format="hh:ii">
					<span class="add-on"><i class="icon-time"></i></span>
					<input type="text" value="" size="13" readonly>
				   </div>
				   <input type="hidden" id="horafin_curso" name="horafin_curso"/><br/>
			      </div>
			 </div>
		    </div>
		    <div class="row-fluid">
			 <div class="span2">
			      <span>Días:</span>
			 </div>
			 <div class="span10 input-block-level" title="Seleccione los dias del Curso">
			     Lunes <input type="checkbox" name="dias_curso[]" value="Lunes"> 
			     Martes <input type="checkbox" name="dias_curso[]" value="Martes">  
			     Miercoles <input type="checkbox" name="dias_curso[]" value="Miercoles"> 
			     Jueves <input type="checkbox" name="dias_curso[]" value="Jueves">  
			     Viernes <input type="checkbox" name="dias_curso[]" value="Viernes">  
			     Sabado <input type="checkbox" name="dias_curso[]" value="Sabado">
			 </div>
		    </div>
		    <div class="row-fluid"><hr />
			 <div class="row-fluid">
			      <div class="span12">
				  <div class="span2"><span>Cúpos:</span></div>
				  <div class="span1">
				      <input name="cupo_curso" type="number" class="input-block-level" min="3" max="20" placeholder="20" title="Ingrese los cupos del curso " required>
				  </div>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12">
				   <div class="span2"><span>Costo:</span></div>
					<div class="span3 input-prepend input-append">
					     <span class="add-on">Bs F.</span>
					     <input name="costo_curso" type="text" size="5" maxlength="4" placeholder="100" title="Ingrese el precio del curso"  pattern="[0-9]{0,}" > Bs f.
					     <span class="add-on">.00</span>
					</div>
				   </div>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12">
				   <div class="span2"><span>Instructor:</span></div>
				   <div class="span6">
					<select name="ci_instru" title="Seleccione un Instructor" required>
					     <option value="">Seleccione...</option>
					     <?PHP
						  $busq_m=mysql_query("select * from sme.instructor WHERE estado=0 ORDER BY ci_instru");
						  while($reg_m=mysql_fetch_array($busq_m))
						  {
						  echo "<option value='".$reg_m['ci_instru']."' >".$reg_m['nombre_instru']." ".$reg_m['apellido_instru']."</option>";
						  }			
					     ?>
					</select>
				   </div>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12">
				   <div class="span2"><span>Certificado:</span></div>
				   <div class="span6">
				       Si <input type="radio" name="certi_curso" value="Si" checked> No <input type="radio" name="certi_curso" value="No">
				   </div>
			      </div>
			 </div><hr><br />
			 <div class="row-fluid">
			      <div class="span12 text-center">
				   <button type="submit" class="btn btn-primary"><i class="icon-hdd icon-white"></i> Registrar</button>
				   <button type="reset" class="btn btn-warning"><i class="icon-trash icon-white"></i> Limpiar</button>
			      </div>
			 </div>
		    </form><!--cierre del formulario-->
	       </div><!--cierre del Hero-Unit-->
	  </div><!--cierre del contenido-->
     </div><!--cierre del row-fluid de contenido-->
</div><!--cierre del container-->
<?php


}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permisos para ingresar al registro de cursos.');
                      document.location=('index.php');
                  </script>";
		  
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
require('piepagina.php');
?>