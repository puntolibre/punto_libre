<?php
     require('cabecera.php');
     require('menu.php');
      
      $con= @mysql_connect("localhost","root","21183652");
     
     if(!mysql_select_db("administracion", $con)){
         echo "error al conectarse";}
         include("../../Sistema_administrativo/php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Acceso_Cursos']==1 and $_SESSION['Modificar_Informacion']==1){
     
     require('conexion.php');
     
     $cod_curso = $_GET['cod_curso'];
     $sql = "SELECT * FROM curso, instructor, horario WHERE curso.cod_curso=horario.cod_curso and curso.ci_instru=instructor.ci_instru and curso.cod_curso=$cod_curso LIMIT 0,1";

     $result = mysql_query($sql);
     $row = mysql_fetch_assoc($result);
?>
	  <div class="span9"><!--Contenido-->
	       <div class="hero-unit"><!--Bloque de Contenido Gris-->
		    <h3 class="text-center">Modificar Curso</h3>
		    <form method="post" action="curso_actualizar.php"><!--incio de formulario-->
			 <div class="row-fluid">
			      <div class="span12 text-center btn-primary">
				   <span>Datos del Curso</span>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12"><br />
				   <div class="row-fluid">
				       <div class="span2"><span>Código:</span></div>
				       <div class="span1">
					   <input name="cod_curso" class="input-block-level text-center" type="text" readonly value="<?=$row['cod_curso']?>">
				       </div>
				   </div>
				   <div class="row-fluid">
					<div class="span2"><span>Curso:</span></div>
					<div class="span7">
					    <input name="nombre_curso" type="text" class="input-block-level" maxlength="50" value="<?=$row['nombre_curso']?>" pattern="[A-Z\a-z\á,é,í,ó,ú ]{0,}[5]{0,1}" required>
					</div>
				   </div>
				   <div class="row-fluid">
					<div class="span2"><span>Imagen:</span></div>
					<div class="span2 borde1">
					      <img src="<?=$row['img_curso']?>"/>
					</div>
				   </div><br>
				   <div class="row-fluid">
					<div class="span2"><span>Descripción:</span></div>
					<div class="span7">					    
					    <textarea name="descripcion_curso" class="input-block-level" rows="5" title="Ingrese la descripcion del curso" required><?=$row['descripcion_curso']?></textarea>
					</div>
				   </div>
				   <div class="row-fluid">
					<div class="span2"><span>Temas:</span></div>
				        <div class="span7">					    
					   <textarea name="tema_curso" class="input-block-level" rows="5" title="Ingrese los temas del curso" required><?=$row['tema_curso']?></textarea>
				        </div>
				   </div>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12 text-center btn-primary">
				   <span>Horario del Curso</span>
			      </div>
			 </div><br />
			 <div class="row-fluid">
			      <div class="span2">
				   <span>Fecha inicio:</span>
			      </div>
			      <div class="span3">
				   <div class="control-group">
					<div class="controls input-append date form_date" data-date="<?php echo date('Y-m-d');?>" data-date-format="dd MM yyyy" data-link-field="fechaini_curso" data-link-format="yyyy-mm-dd">
					<input type="text" size="10" value="<?=$row['fechaini_curso']?>" class="input-block-level" readonly>
					     <span class="add-on"><i class="icon icon-calendar"></i></span>
					</div>
					<input type="hidden" id="fechaini_curso" value="<?=$row['fechaini_curso']?>" name="fechaini_curso"/><br/>
				   </div>
			      </div>
			      <div class="span2">
				   <span>Fecha Fin:</span>
			      </div>	 
			      <div class="span4">
				   <div class="control-group">
					<div class="controls input-prepend date form_date" data-date="<?php echo date('Y-m-d');?>" data-date-format="dd MM yyyy" data-link-field="fechafin_curso" data-link-format="yyyy-mm-dd">
					<span class="add-on"><i class="icon icon-calendar"></i></span>
					     <input type="text" size="10" value="<?=$row['fechafin_curso']?>" class="input-block-level" readonly>
					</div>
					<input type="hidden" id="fechafin_curso" name="fechafin_curso" value="<?=$row['fechafin_curso']?>"/><br/>
				   </div>       
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span2">
				   <span>Hora Inicio:</span>
			      </div>
			      <div class="span3">
				   <div class="control-group">
					<div class="controls input-append date form_time" data-date="" data-date-format="hh:ii:ss"  data-link-field="horaini_curso" data-link-format="hh:ii:ss">
					<input type="text" value="<?=$row['horaini_curso']?>" size="10"readonly>
					     <span class="add-on"><i class="icon-time"></i></span>
					</div>
					<input type="hidden" id="horaini_curso" name="horaini_curso" value="<?=$row['horaini_curso']?>" class="input-block-level"/><br/>
				   </div>
			      </div>
			      <div class="span2">
				   <span>Hora Fin:</span>
			      </div>
			      <div class="span4">
				   <div class="control-group">
				   <div class="controls input-prepend date form_time" data-date="" data-date-format="hh:ii" data-link-field="horafin_curso" data-link-format="hh:ii">
					<span class="add-on"><i class="icon-time"></i></span>
					<input type="text" value="<?=$row['horafin_curso']?>" size="10" readonly>
				   </div>
				   <input type="hidden" id="horafin_curso" name="horafin_curso" value="<?=$row['horafin_curso']?>"/><br/>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12">
				   <div class="span2">
				       <span>Dias:</span>
				   </div>
				   <div class="span6" title="Dias seleccionado">
				       <legend><?=$row['dias_curso']?></legend>
				   </div>
			      </div>
			      <div class="row-fluid">
				   <div class="span2"></div>
				   <div class="span10" title="Indique los dias seleccionados anteriormente o seleccione los nuevos dias en caso de un cambio">
				       Lunes <input type="checkbox" name="dias_curso[]" value="Lunes"
				       <?php if ($row['dias_curso[]'] == "Lunes") echo "checked";?>/>
	       
				       Martes <input type="checkbox" name="dias_curso[]" value="Martes"
				       <?php if ($row['dias_curso[]'] == "Martes") echo "checked";?>/>
				       
				       Miercoles <input type="checkbox" name="dias_curso[]" value="Miercoles"
				       <?php if ($row['dias_curso[]'] == "Miercoles") echo "checked";?>/> 
				       
				       Jueves <input type="checkbox" name="dias_curso[]" value="Jueves"
				       <?php if ($row['dias_curso[]'] == "Jueves") echo "checked";?>/>
				       
				       Viernes <input type="checkbox" name="dias_curso[]" value="Viernes"
					<?php if ($row['dias_curso[]'] == "Viernes") { echo "checked=\"checked\"";} ?> />  
				       
				       Sabado <input type="checkbox" name="dias_curso[]" value="Sabado"
					<?php if ($row['dias_curso[]'] == "Sabado") { echo "checked=\"checked\"";} ?> />
				   </div>
			      </div>
			 </div><hr />
			 <div class="row-fluid">
			      <div class="span12">
				  <div class="span2"><span>Cupos:</span></div>
				  <div class="span1">
				      <input name="cupo_curso" type="number" class="input-block-level" min="3" max="20" value="<?=$row['cupo_curso']?>" requerid>
				  </div>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12">
				   <div class="span2"><span>Costo:</span></div>
				   <div class="span6">
					<input name="costo_curso" type="text" size="5" maxlength="4" value="<?=$row['costo_curso']?>"> Bs f.
				   </div>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12">
				   <div class="span2"><span>Instructor:</span></div>
				   <div class="span6">
					<select name="ci_instru" title='seleccione un instructor'">
					     <option value="<?=$row['nombre_instru']?><?=$row['apellido_instru']?>">Seleccione...</option>
					     <?PHP
						  $busq_m=mysql_query("select * from sme.instructor ORDER BY ci_instru");
						  while($reg_m=mysql_fetch_array($busq_m))
						  {
						  if ($row['ci_instru']==$reg_m['ci_instru'])
						  echo "<option selected value='".$reg_m['ci_instru']."' >".$reg_m['nombre_instru']." ".$reg_m['apellido_instru']."</option>";
						  else
						  echo "<option value='".$reg_m['ci_instru']."' >".$reg_m['nombre_instru']." ".$reg_m['apellido_instru']."</option>";
						  }			
					     ?>
					</select>
				   </div>
			      </div>
			 </div>
			 <div class="row-fluid">
			      <div class="span12">
				   <div class="span2"><span>Certificado:</span></div>
				   <div class="span6">
				       Si <input type="radio" name="certi_curso" value="Si"
				       <?php if ($row['certi_curso'] == "Si") { echo "checked=\"checked\"";} ?> onClick="selec()"/>
				       No <input type="radio" name="certi_curso" value="No"
				       <?php if ($row['certi_curso'] == "No") { echo "checked=\"checked\"";} ?> onClick="selec()"/>
				   </div>
			      </div>
			 </div><hr><br />
			 <div class="row-fluid">
			      <div class="span12 text-center">
				   <button type="submit" class="btn btn-primary"><i class="icon-pencil icon-white"></i> Actualizar</button>
				   <a class="btn" href="curso_descripcion.php?codigo=<?=$row['cod_curso']?>" role="button"><i class="icon-hand-left"></i> Volver</a>
			      </div>
			 </div>
		    </form><!--cierre del formulario-->
	       </div><!--cierre del Hero-Unit-->
	  </div><!--cierre del contenido-->
     </div><!--cierre del row-fluid de contenido-->
</div><!--cierre del container-->
<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para modificar los cursos.');
                      document.location=('index.php');
                  </script>";
		  
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
?>