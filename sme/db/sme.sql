-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 15-06-2014 a las 23:14:11
-- Versión del servidor: 5.5.37
-- Versión de PHP: 5.4.4-14+deb7u9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sme`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
  `cod_curso` int(3) NOT NULL AUTO_INCREMENT,
  `nombre_curso` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `img_curso` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_curso` text COLLATE utf8_unicode_ci NOT NULL,
  `tema_curso` text COLLATE utf8_unicode_ci NOT NULL,
  `cupo_curso` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `costo_curso` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `certi_curso` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `ci_instru` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cod_curso`),
  KEY `ci_instru` (`ci_instru`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`cod_curso`, `nombre_curso`, `img_curso`, `descripcion_curso`, `tema_curso`, `cupo_curso`, `costo_curso`, `certi_curso`, `ci_instru`) VALUES
(6, 'Curso de JavaScript', 'img/cursos/6.png', 'Prueba', 'Prueba', '20', '100', 'Si', '21394646');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE IF NOT EXISTS `horario` (
  `cod_curso` int(3) NOT NULL,
  `fechaini_curso` date NOT NULL,
  `fechafin_curso` date NOT NULL,
  `horaini_curso` time NOT NULL,
  `horafin_curso` time NOT NULL,
  `dias_curso` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  KEY `cod_curso` (`cod_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`cod_curso`, `fechaini_curso`, `fechafin_curso`, `horaini_curso`, `horafin_curso`, `dias_curso`) VALUES
(6, '2014-06-18', '2014-06-28', '21:43:00', '21:43:00', 'Martes, Jueves');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscriben`
--

CREATE TABLE IF NOT EXISTS `inscriben` (
  `cod_curso` int(3) NOT NULL,
  `ci_part` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `ci_part` (`ci_part`),
  KEY `cod_curso` (`cod_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `inscriben`
--

INSERT INTO `inscriben` (`cod_curso`, `ci_part`) VALUES
(6, '21394646');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructor`
--

CREATE TABLE IF NOT EXISTS `instructor` (
  `ci_instru` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_instru` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellido_instru` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `genero_instru` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `direccion_instru` text COLLATE utf8_unicode_ci NOT NULL,
  `tlfn_instru` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `correo_instru` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ocupacion_instru` enum('Estudiante','Profesional') COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`ci_instru`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `instructor`
--

INSERT INTO `instructor` (`ci_instru`, `nombre_instru`, `apellido_instru`, `genero_instru`, `direccion_instru`, `tlfn_instru`, `correo_instru`, `ocupacion_instru`, `estado`) VALUES
('21394646', 'Miguel Antonio ', 'Sanabria Camacho', 'Masculino', 'qwerty', '0414-3761753', '', 'Profesional', 0),
('23724395', 'Andy Alberto ', 'Torres Avila', 'Masculino', 'La parroquia Municipio Libertador', '0414-3761753', '', 'Estudiante', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participante`
--

CREATE TABLE IF NOT EXISTS `participante` (
  `ci_part` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_part` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellido_part` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `genero_part` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `direccion_part` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tlfn_part` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `correo_part` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ocup_part` enum('Estudiante','Profesional') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ci_part`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `participante`
--

INSERT INTO `participante` (`ci_part`, `nombre_part`, `apellido_part`, `genero_part`, `direccion_part`, `tlfn_part`, `correo_part`, `ocup_part`) VALUES
('21394646', 'Miguel Antonio', 'Sanabria Camacho', 'Masculino', 'Merida', '0416-5753623', 'masc1293@hotmail.com', 'Estudiante'),
('7940000', 'Rosalba Perez', 'Perez Rojas', 'Femenino', 'Calle El Porvenir', '0274-2215668', 'rossyger17@hotmail.com', 'Profesional');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `curso`
--
ALTER TABLE `curso`
  ADD CONSTRAINT `curso_ibfk_1` FOREIGN KEY (`ci_instru`) REFERENCES `instructor` (`ci_instru`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `horario`
--
ALTER TABLE `horario`
  ADD CONSTRAINT `horario_ibfk_1` FOREIGN KEY (`cod_curso`) REFERENCES `curso` (`cod_curso`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inscriben`
--
ALTER TABLE `inscriben`
  ADD CONSTRAINT `inscriben_ibfk_1` FOREIGN KEY (`cod_curso`) REFERENCES `curso` (`cod_curso`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inscriben_ibfk_2` FOREIGN KEY (`ci_part`) REFERENCES `participante` (`ci_part`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
