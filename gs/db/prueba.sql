-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-05-2014 a las 21:41:19
-- Versión del servidor: 5.5.37-MariaDB
-- Versión de PHP: 5.5.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP SCHEMA IF EXISTS `prueba` ;
CREATE SCHEMA IF NOT EXISTS `prueba` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci ;
USE `prueba` ;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `nombre_usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_usu` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_usu` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `especialidad` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`nombre_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`nombre_usuario`, `nombre_usu`, `apellido_usu`, `especialidad`) VALUES
('andi', 'Andy ', 'Torres', 'Canaimitas'),
('jesus', 'Jesús', 'Vielma', 'Contenido educativo'),
('juan_23', 'Juan', 'Peña', 'Canaimitas'),
('masc1293', 'Miguel', 'Sanabria', 'Canaimitas'),
('pedro_perez', 'Pedro', 'Perez', 'laptops');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
