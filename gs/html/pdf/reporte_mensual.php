<?php
require('fpdf17/fpdf.php');


class PDF extends FPDF
{
// Cabecera de p�gina
function Header()
{
	// Logo
	$this->Image('PL.png',10,8,33);
	// Arial bold 15
	$this->SetFont('Times','',15);
	// T�tulo
 $this->Ln(5); //Saltos de Linea
 // Movernos a la derecha
 $this->Cell(80);
 //Texto
	$this->Cell(60,10,'GNU de Venezuela',0,0,'C');
 $this->Ln(5);  //Saltos de Linea
 $this->Cell(80);
 $this->Cell(60,10,'Punto Libre M�rida',0,0,'C');
 $this->Image('gnu.png',230,9,33);
	// Salto de l�nea
	$this->Ln(25);
}

// Pie de p�gina
function Footer()
{
	// Posici�n: a 1,5 cm del final
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Times','I',8);
	// N�mero de p�gina
	$this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
}
function Firma()
{
	// Posici�n: a 1,5 cm del final
	$this->SetY(-50);
	// Arial italic 8
	$this->SetFont('Times','B',13);
	// N�mero de p�gina
 
	$this->Cell(50,10,'Firma del receptor ',0,0);
 $this->Cell(70);
 $this->Cell(50,10,'Firma del Cliente',0,0);
}
}
include('../conexion.php');
$cod_soporte=$_GET['cod_soporte'];
$sql=mysql_query("SELECT  SELECT  `dueno`.`nombre` ,  `dueno`.`apellido` ,  `dueno`.`nacionalidad` ,  `dueno`.`cedula` ,  `equipo`.`tipo_equipo` ,  `equipo`.`mem_ram` ,  `equipo`.`disco_duro` , `soporte`.`cod_soporte`, `soporte`.`tipo_soporte`, `soporte`.`descripcion_soporte`,
                 Date_Format(`soporte`.`fecha_recepcion`,'%d-%m-%Y') AS fecha_recepcion, `soporte`.`responsable`, `estado`.`estado`, MONTH(NOW())
FROM dueno
INNER JOIN equipo ON  `equipo`.`cedula` =  `dueno`.`cedula` 
INNER JOIN soporte ON  `soporte`.`cod_soporte` =  `equipo`.`cod_soporte` 
INNER JOIN estado ON `estado`.`cod_soporte` = `soporte`.`cod_soporte`
WHERE  MONTH(`fecha_recepcion`)=MONTH(NOW())");
$row=mysql_fetch_array($sql);
$date=getdate();
// Creaci�n del objeto de la clase heredada
$pdf = new PDF('L','mm','Letter');
$pdf->SetMargins('30','30','30');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Cell(70);
$pdf->Cell(60,10,'Reporte de equipos recibidos en el mes.',0,1,'');
$pdf->Ln(2);
$pdf->Cell(60,10,$row['apellido'],1,1);

$pdf->Output('soporteNo_'.$cod_soporte,'I');
?>
