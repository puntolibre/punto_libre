
	<head>
		<meta charset="utf-8" />
		<title>Gestión de Soporte</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<meta name="description" content="">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
		<link href="../css/bootstrap-responsive.css" rel="stylesheet">
		<link href="../css/bootstrap-datetimepicker.css" rel="stylesheet">

	</head> 
<div class="container-fluid">
<br><br>
      <form class="form-signin" action="sesion.php" method="post" autocomplete="off">
        <h2 class="form-signin-heading">Inicie Sesión</h2>
        <div class="input-prepend">
         <span class="add-on"><i class="icon-user"></i></span>
         <input type="text" class="input-block-level" placeholder="Nombre de Usuario" name="login" autofocus> 
        </div>
        <div class="input-prepend">
         <span class="add-on"><i class="icon-lock"></i></span>
        <input type="password" class="input-block-level" placeholder="Contraseña" name="pass">
         </div>
        <br>
        <button class="btn btn-large btn-primary" type="submit">Entrar</button>
        <p class="muted">¿No tiene una cuenta de Usaurio? <a href="#" class="disabled" >Registrese</a></p>
      </form>
      
    </div>

<?php
require('pie.php');
?>