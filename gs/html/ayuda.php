<?php
require('cabecera.php');
require('menu_lateral.php');
?>
				<div class="span9">
					<div class="hero-unit">
						<h2 class="text-center">Ayuda</h2>
						<p>Aqui tienes la principales Preguntas y respuestas de como usar el Sistema de Gestión de Soporte a Equipos.
						</p>
				<div class="navbar ">
			<div class="navbar-inner">
				<div class="container-fluid">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
					 </button>
					<span class="brand">Menu de Ayuda</span>
					<div class="nav-collapse collapse">
						<ul class="nav ">
								<li class="dropdown">
								<a href="registrosoporte.html" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-question-sign"></i> Preguntas Frecuentes <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li ><a href="#pregunta1" ><i class="icon-question-sign"></i> Pregunta 1</a></li>
									<li><a href="#pregunta2"><i class="icon-question-sign"></i> Pregunta 2</a></li>
									<li><a href="#pregunta3"><i class="icon-question-sign"></i> Pregunta 3</a></li>
								</ul>
							</li>
								<li><a href="#"><i class="icon-book"></i> Manual del Usuario</a></li>
							<li><a href="creditos.php"><i class="icon-bookmark"></i> Creditos</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
<br><br>
						<h3>Preguntas Frecuentes</h3>
						<div class="well">
								<a id="pregunta1"></a>
						<p><h4>Pregunta</h4></p>
						<p>Respuesta:
						<br>
						Las respuestas seran lo mas cortas posibles indicadole al usuario la mayor información posible para resolver la duda que presenta.
						En algunos casos y de ser necesario se proporcionara una imagen para que el usuario pueda entender mejor la respuesta.
						</p>
						¿Fue de ayuda la respuesta?
						<br>
							<button class="btn btn-primary" name="ayuda">Si</button>
							<button class="btn btn-danger" name="ayuda">No</button>
						<a href="#top" class="pull-right"><i class="icon-arrow-up"></i> Volve al Principio</a>
					</div>
						<div class="well">
								<a id="pregunta2"></a>
						<p><h4>Pregunta</h4></p>
						<p>Respuesta:
						<br>
						Las respuestas seran lo mas cortas posibles indicadole al usuario la mayor información posible para resolver la duda que presenta.
						En algunos casos y de ser necesario se proporcionara una imagen para que el usuario pueda entender mejor la respuesta.
						</p>
						¿Fue de ayuda la respuesta?
						<br>
						<button class="btn btn-primary" name="si">Si</button>
						<button class="btn btn-danger" name="no">No</button>
								<a class="pull-right" href="#top"><i class="icon-arrow-up"></i> Volve al Principio</a>
						</div>
					<div class="well">
								<a id="pregunta3"></a>
						<p><h4>Pregunta</h4></p>
						<p>Respuesta:
						<br>
						Las respuestas seran lo mas cortas posibles indicadole al usuario la mayor información posible para resolver la duda que presenta.
						En algunos casos y de ser necesario se proporcionara una imagen para que el usuario pueda entender mejor la respuesta.
						</p>
						¿Fue de ayuda la respuesta?
						<br>
						<button class="btn btn-primary" name="si">Si</button>
						<button class="btn btn-danger" name="no">No</button>
								<a href="#top" class="pull-right"><i class="icon-arrow-up"></i> Volve al Principio</a>
					</div>
				</div>
				</div>
				
			</div>
			
		</div>
<?php
require('pie.php');
?>