<?php
require('cabecera.php');
?>
<div class="container-fluid">
	<div class="row-fluid">
	<div class="row-fluid">
            <h2>Creditos</h2>
            <p>Este sistema fue realizado en la Universidad Politecnica Territorial del Estado Mérida (UPTMKR)</p>
            <strong><p>Bajo la Tutoria de:</p></strong>
         </div>
         <div class="row-fluid">
            <div class="span12">
                <div class="span6 well">
                    <h4>Tutores Academicos</h4>
                </div>
                <div class="span6 well">
                    <h4>Tutores en la comunidad</h4>
                </div>
            </div>
         </div>
         <div class="row text-center">
            <strong><p>Fue desarrollado por:</p></strong>
         </div>
         <div class="row-fluid">
            <div class="span12">
                <div class="span6 well">
                    <h4>Módulo: Administración de Punto Libre</h4>
                </div>
                <div class="span6 well">
                    <h4>Módulo: Gestión de Cursos</h4>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="span6 well">
                    <h4>Módulo: Organización Inventario</h4>
                </div>
                <div class="span6 well">
                    <h4>Módulo: Gestión de Soporte a Equipos</h4>
                    <p>EL módulo de gestión de soporte a equipos fue desarrollado los estudiantes:</p>
                    <div class="row-fluid thumbnail">
                        <div class="span4">
                            <div class="thumbnail">
                            <img src="../img/personas/image10.png" alt="hola">
                            </div>
                            <div class="caption">
                            <h4>Jesús Vielma</h4>
                            <a href="https://twitter.com/chuyquien" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @chuyquien</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<br>
<a href="//plus.google.com/108178194666820594837?prsrc=3"
   rel="publisher" target="_top" style="text-decoration:none;display:inline-block;color:#333;text-align:center; font:13px/16px arial,sans-serif;white-space:nowrap;">
<span style="display:inline-block;font-weight:bold;vertical-align:top;margin-right:5px; margin-top:8px;">Jesús Vielma</span><span style="display:inline-block;vertical-align:top;margin-right:15px; margin-top:8px;">on</span>
<img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
</a>
                            </div>
                        </div>
                        <!--<div class="span4">
                            <div class="thumbnail">
                            <img src="../img/personas/image11.png" alt="hola">
                            </div>
                            <div class="caption">
                            <h4>Engelbert Portillo</h4>
                            <a href="https://twitter.com/alexangelbert" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @alexangelbert</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </div>
                        </div>-->
                        <div class="span4">
                            <div class="thumbnail">
                            <img src="../img/personas/image12.png" alt="hola">
                            </div>
                            <div class="caption">
                            <h4>Pedro Peralta</h4>
                            <a href="https://twitter.com/PetterV27" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @PetterV27</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
								
<?php
require('pie.php');
?>
